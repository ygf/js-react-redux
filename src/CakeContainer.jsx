import React from 'react'
import { connect } from 'react-redux'
import buyCake from './redux/cakes/cakeActions'

function CakeContainer(props) {
    return (
        <div>
            <p>蛋糕数量: {props.numOfCakes}</p>
            <button onClick={props.buyCake}>购买蛋糕</button>
        </div>
    )
}

const mapStateToProps = state => {
    return {
        numOfCakes: state.numOfCakes
    }
}

const mapDispatchToProps = dispatch => {
    return {
        buyCake: () => dispatch(buyCake())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CakeContainer)
